# Security Games Online

All the material necessary to play security game like
* Elevation of Privilege (EoP) 
* OWASP Cornucopia

online, with the help of an collaboration tool like

* Collaboard https://www.collaboard.app 
* Miro
* Mural
* ...

## The cards

* OWASP Cornucopia 
** https://owasp.org/www-project-cornucopia/
** Cards https://drive.google.com/open?id=0ByNJ8mfWALwjNXpQMUNBYnJsT2QyQ0lkb3VNX1BCM3JLNlBZ
* EoP (with privacy extension)
** https://logmeincdn.azureedge.net/legal/gdpr-v2/eop-cards-ready-to-print.pdf
