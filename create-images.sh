#! /bin/bash

BASENAME="EoP-Cards/eop-%03d.png"

SELECTION=$(seq 10 2 182 | tr "\n" , )
SELECTION=${SELECTION%?}
convert -density 150 "eop-cards-ready-to-print.pdf[$SELECTION]" $BASENAME

convert -density  300 "cornucopia-ecom-1v20en-owasp/OWASP CORNUCOPIA Playing Cards v1.20-EN.pdf[1-81]" "Cornucopia-Cards/cornucopia-%02d.png"

