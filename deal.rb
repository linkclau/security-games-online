#! /usr/bin/env ruby


require 'optparse'
options = {}

optparse = OptionParser.new do|opts|
   # Set a banner, displayed at the top
   # of the help screen.
   opts.banner = "Usage: #{$0} [options] file1 file2 "
   opts.on( '-p <number>', '--nplayer=<number>', 'Number of players' ) do |n|
     options[:nPlayers] = n.to_i
   end
   opts.on( '-c <number>', '--ncards=<number>', 'Cards dealt to each players' ) do |n|
     options[:nCards] = n.to_i
   end
   opts.on( '-d', '--target-dir=<output directory>', 
           'Were to create the output structure' ) do |n|
     options[:target_directory] = n
   end
   opts.on( '-s', '--seed=<entropy>', 
           'Seed for the random number generator' ) do |n|
     options[:seed] = n.to_i
   end
   opts.on( '-h', '--help', 'Display this screen' ) do
     puts opts
     exit
   end
end

optparse.parse! ARGV

cards = ARGV


# Check mandatory options
[ :nPlayers, :target_directory].each{ |o|
  if not options.key?(o) 
    STDERR.puts ""
    STDERR.puts "#{$0} Error: mandatory option (#{o.to_s}) missing!"
    STDERR.puts ""
    STDERR.puts optparse.help
    STDERR.puts ""
    exit 1
  end
}
nPlayers = options[:nPlayers]
nCards = options[:nCards]
target_directory = options[:target_directory]
seed = options.key?(seed) ? options[:seed] : Random.srand

g = Random.new seed

puts "Playing with seed: #{seed}"
puts "#Players: #{nPlayers}" + (nCards.nil? ? "" : " - #{nCards} each")


deals = (nCards.nil? ? cards.length : [nPlayers*nCards, cards.length].min )
# Initialize the players
players = {}
(0...nPlayers).each{|p| players[p] = []}

r = 0
while r < deals 
  n = g.rand 0...cards.length
  c = cards.delete_at n
  p = r%nPlayers
  players[p].append c

  r += 1
end


require 'fileutils'

FileUtils.mkdir_p target_directory
(0...nPlayers).each{|p|
  pd = "#{target_directory}/player#{p}"
  puts "#{pd} #{players[p].length} =>  #{players[p]}"
  FileUtils.mkdir_p pd
  FileUtils.cp players[p], pd
}
unless cards.empty?
  stack = "#{target_directory}/stack"
  puts "stack #{cards.length} =>  #{cards[0..5]} ..."
  FileUtils.mkdir_p stack
  FileUtils.cp cards, stack
end



